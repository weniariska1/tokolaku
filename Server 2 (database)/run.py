from flask import Flask, make_response, jsonify

# database ======================================================================================
import mysql.connector
def dbConn(user="root", password="", host="localhost", database="db_tokolaku"):
    db = mysql.connector.connect(
        host=host,
        user=user,
        password=password,
        database=database
    )
    return db

#Get all row
def select_all(query, values, conn):
    mycursor = conn.cursor(buffered=True)
    mycursor.execute(query, values)
    row_headers = [x[0] for x in mycursor.description]
    myresults = mycursor.fetchall()
    json_data = []
    if myresults != None:
        for myresult in myresults:
            result = [(x.decode() if type(x) == bytearray else x)
            for x in myresult]  # decode bytearray
        json_data.append(dict(zip(row_headers, result)))
        return json_data
    else:
        return None

# get single row
def select_row(query, values, conn):
    mycursor = conn.cursor(buffered=True)
    mycursor.execute(query, values)
    row_headers = [x[0] for x in mycursor.description]
    myresult = mycursor.fetchone()
    if myresult != None:
        result = [(x.decode() if type(x) == bytearray else x)
            for x in myresult]  # decode bytearray
        return dict(zip(row_headers, result))
    else:
        return None

# insert dan update
def inup(query, val, conn):
	mycursor = conn.cursor()
	mycursor.execute(query, val)
	conn.commit()
# end database ======================================================================================

app = Flask(__name__)

@app.route("/get_produk_toko/<id>", methods=['GET'])
def get_produk_toko(id):
    query = "select * from produk where id=%s"
    values = (id, )
    dataProduk = select_row(query, values, dbConn())
    if dataProduk != None:
        query2 = "select * from toko where id=%s"
        values2 = (dataProduk['id_toko'], )
        dataToko = select_row(query2, values2, dbConn())
        return make_response(jsonify({'status_code': 200, 'dataProduk': dataProduk, 'dataToko': dataToko}), 200)
    else:
        return make_response(jsonify({'status_code': 400, 'message': 'Bad request'}), 400)

if __name__ == "__main__":
    app.run(port=8020, debug=True)
