-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2022 at 07:13 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tokolaku`
--

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `id_toko` int(11) NOT NULL,
  `namaProduk` varchar(255) NOT NULL,
  `hargaProduk` float(11,2) NOT NULL,
  `beratProduk` int(11) NOT NULL,
  `stokProduk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `id_toko`, `namaProduk`, `hargaProduk`, `beratProduk`, `stokProduk`) VALUES
(1, 2, 'Beauty Glow Day Cream', 80000.00, 200, 200),
(2, 2, 'Beauty Glow Night Cream', 80000.00, 200, 200),
(3, 1, 'Gamis Anak 10-15 tahun', 185000.00, 230, 50),
(4, 2, 'Beauty Glow Serum', 125000.00, 350, 200),
(5, 1, 'Gamis Sultan Model Terbaru', 385000.00, 300, 20),
(6, 1, 'Gamis Dewasa Model Terbaru', 240000.00, 260, 25),
(7, 6, 'Hoodie ala korea', 75000.00, 300, 50),
(8, 6, 'Hoodie Oversize Kekinian', 95000.00, 400, 50),
(9, 6, 'Hoodie telinga kucing', 60000.00, 290, 50),
(10, 4, 'Gelang nama custom', 25000.00, 150, 300),
(11, 4, 'Kalung nama custom', 45000.00, 250, 300),
(12, 4, 'Kalung IU', 56000.00, 350, 300),
(13, 5, 'Pashmina Diamond', 24000.00, 230, 1000),
(14, 5, 'Pashmina Ceury Baby Doll Premium', 25000.00, 340, 1000),
(15, 5, 'Hijab segi empat voal', 20000.00, 210, 1000),
(16, 3, 'Celana dengan kantong banyak', 80000.00, 400, 500),
(17, 3, 'Kaos oversize hitam ala korea kekinian', 55000.00, 320, 500);

-- --------------------------------------------------------

--
-- Table structure for table `toko`
--

CREATE TABLE `toko` (
  `id` int(11) NOT NULL,
  `namaToko` varchar(100) NOT NULL,
  `id_kota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `toko`
--

INSERT INTO `toko` (`id`, `namaToko`, `id_kota`) VALUES
(1, 'Dinka Gamis', 3),
(2, 'Beauty Glow', 2),
(3, 'Jessi Style', 12),
(4, 'Ryu Beauty', 15),
(5, 'Hijab Modis', 10),
(6, 'Nuansa Remaja', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toko`
--
ALTER TABLE `toko`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `toko`
--
ALTER TABLE `toko`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
