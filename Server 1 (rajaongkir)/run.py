from flask import Flask, make_response, jsonify, request
import requests

app = Flask(__name__)
headers = {
    'key': "ebe570fda3265af7b82f4fab8d1c7025",
    'content-type': "application/x-www-form-urlencoded"
}

@app.route("/get_ongkir", methods=['GET'])
def get_ongkir():
    payload = request.form.to_dict()
    service = payload['service']
    del payload['service']
    url = "https://api.rajaongkir.com/starter/cost"
    response = requests.request('POST', url, data=payload, headers=headers)
    results = response.json()['rajaongkir']['results'][0]['costs']
    result = []
    [(result.append(x) if x['service'].lower() == service.lower() else '') for x in results]
    if len(result) != 0:
        hasil = {
            'service': result[0]['service'],
            'estimasi day' : result[0]['cost'][0]['etd'],
            'ongkir': result[0]['cost'][0]['value']
        }
        return make_response(jsonify({'status_code': 200, 'data': hasil}), 200)
    else:
        return make_response(jsonify({'status_code': 400, 'message': 'Bad request'}), 400)

if __name__ == "__main__":
    app.run(port=8010, debug=True)
