from flask import Flask, make_response, jsonify, request
import requests

app = Flask(__name__)

@app.route("/make_order", methods=['POST'])
def make_order():
    data = request.form.to_dict()
    url_server2 = "http://127.0.0.1:8020/get_produk_toko/"
    response2 = requests.request('GET', url_server2+data['id_produk'])
    hasil2 = response2.json()

    url_server1 = "http://127.0.0.1:8010/get_ongkir"
    weight = int(hasil2['dataProduk']['beratProduk'])*int(data['jumlah_pembelian'])
    payload1 = {
        'origin': data['id_city'],
        'destination': hasil2['dataToko']['id'],
        'weight': weight,
        'courier' : data['jasa_kirim'],
        'service': data['layanan_jasa_kirim']
    }
    response1 = requests.request('GET', url_server1, data=payload1)
    detailOngkir = response1.json()['data']
    detailOngkir['Jasa Kirim'] = data['jasa_kirim']
    totalharga = (int(hasil2['dataProduk']['hargaProduk'])*int(data['jumlah_pembelian']))+int(detailOngkir['ongkir'])

    results = {
        'detailProduk': hasil2['dataProduk'],
        'detailToko': hasil2['dataToko'],
        'detailOngkir': detailOngkir,
        'jumlahPembelian': data['jumlah_pembelian'],
        'totalHarga': totalharga
    }
    return make_response(jsonify({'status_code': 200, 'results': results}), 200)

if __name__ == "__main__":
    app.run(port=8000, debug=True)
